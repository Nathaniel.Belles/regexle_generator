from http.server import BaseHTTPRequestHandler
import random
import string
import copy
import math
import re
import json
from urllib.parse import urlparse, parse_qs
import logging
import sys
import os

logging.basicConfig(
    level=logging.DEBUG,
    format="%(levelname)s:%(pathname)s:%(lineno)d: %(message)s",
)


class RegexGenerator:
    REGEX_RULES_TEMPLATE = [
        ".*",
        ".*{center}.*",
        ".*{center}.*{after_substr}.*",
        ".*{before_substr}.*{center}.*",
        ".*{before_substr}.*{center}.*{after_substr}.*",
        "({before_str}|{center}|{after_str})+",
        "({center}|{after_str}|{before_str})+",
        "({after_str}|{before_str}|{center})+",
        "({}|{before_str}|{center}|{after_str})+",
        "({}|{center}|{after_str}|{before_str})+",
        "({}|{after_str}|{before_str}|{center})+",
        "({before_str}|{}|{center}|{after_str})+",
        "({center}|{}|{after_str}|{before_str})+",
        "({after_str}|{}|{before_str}|{center})+",
        "({before_str}|{center}|{}|{after_str})+",
        "({center}|{after_str}|{}|{before_str})+",
        "({after_str}|{before_str}|{}|{center})+",
        "({before_str}|{center}|{after_str}|{})+",
        "({center}|{after_str}|{before_str}|{})+",
        "({after_str}|{before_str}|{center}|{})+",
        # "({full_str})+",
        # "({}|{full_str})+",
        # "({full_str}|{})+",
        # "({}|{}|{full_str})+",
        # "({}|{full_str}|{})+",
        # "({full_str}|{}|{})+",
        # "({}|{}|{}|{full_str})+",
        # "({}|{}|{full_str}|{})+",
        # "({}|{full_str}|{}|{})+",
        # "({full_str}|{}|{}|{})+",
    ]

    def __init__(
        self,
        seed: int,
        difficulty: float = 0.5,
    ) -> None:
        random.seed(seed)
        self.seed = seed
        self.difficulty = difficulty

    def random_regex(self, full_str: str) -> str:
        """
        Generates a random regex rule based on a given full string.

        Args:
            full_str (str): The full string to generate the regex rule from.

        Returns:
            str: A randomly generated regex rule.
        """

        match_length = math.ceil(
            self.difficulty * len(full_str)
        )  # This needs some randomness added
        position = random.randint(0, len(full_str) - match_length)

        center_string = full_str[position : position + match_length]
        before_str = full_str[:position]
        before_substr = self.random_substring(before_str)
        after_str = full_str[position + match_length :]
        after_substr = self.random_substring(after_str)

        # Pick random regex rule
        rule = random.choice(self.REGEX_RULES_TEMPLATE)

        if len(before_str) == 0:
            before_str = self.random_string()
        if len(after_str) == 0:
            after_str = self.random_string()
        logging.debug(
            str(
                [
                    center_string,
                    after_str,
                    before_str,
                    after_substr,
                    before_substr,
                    full_str,
                ]
            )
        )
        new_str = self.no_repetition_random_string(
            [
                center_string,
                after_str,
                before_str,
                after_substr,
                before_substr,
                full_str,
            ],
            len(full_str),
        )

        # Fill in selected string
        rule = rule.format(
            new_str,
            center=center_string,
            after_substr=after_substr,
            before_substr=before_substr,
            before_str=before_str,
            after_str=after_str,
            full_str=full_str,
        )
        rule = self.middle_question(rule)
        rule = self.end_question(rule)
        rule = self.add_brackets(rule)
        rule = self.remove_duplicate_chars(rule)
        rule = self.remove_double_asterisks(rule)

        logging.debug(f"{rule} for {full_str}")
        return rule

    def random_substring(self, string: str) -> str:
        if len(string) == 0:
            return string
        lower = random.randint(0, len(string) - 1)
        upper = random.randint(lower, len(string))

        substring = string[lower:upper]

        return substring

    def random_string(self, length: int = 1) -> str:

        return "".join(random.choices(string.ascii_uppercase, k=length))

    def no_repetition_random_string(self, strings: list[str], full_str_len: int) -> str:
        repeat = True

        while repeat:
            candidate = self.random_string(
                length=random.randint(1, max(1, full_str_len - 1))
            )
            repeat = candidate in strings

        return candidate

    def middle_question(self, rule: str) -> str:
        """
        Applies transformations to the input rule string by replacing specific patterns.

        1. Replaces the first occurrence of 'MY|.*|Y' with 'M?Y'.
        2. Replaces the first occurrence of 'MY|Y' with 'M?Y'.
        3. Replaces the first occurrence of 'Y|.*|MY' with 'M?Y'.
        4. Replaces the first occurrence of 'Y|MY' with 'M?Y'.

        Args:
            rule (str): The input rule string to transform.

        Returns:
            str: The transformed rule string.
        """
        # Find all occurrences of MY|.*|Y and replace with M?Y
        rule = re.sub(
            r"([\|\(])([A-Za-z])([A-Za-z])\|(.*)\|\3([\|\)])",
            r"\1\2?\3|\4\5",
            rule,
            count=1,
        )
        # Find all occurrences of MY|Y and replace with M?Y
        rule = re.sub(
            r"([\|\(])([A-Za-z])([A-Za-z])\|\3([\|\)])",
            r"\1\2?\3\4",
            rule,
            count=1,
        )

        # Find all occurrences of Y|.*|MY and replace with M?Y
        rule = re.sub(
            r"([\|\(])([A-Za-z])\|(.*)\|([A-Za-z])\2([\|\)])",
            r"\1\4?\2|\3\5",
            rule,
            count=1,
        )
        # Find all occurrences of Y|MY and replace with M?Y
        rule = re.sub(
            r"([\|\(])([A-Za-z])\|([A-Za-z])\2([\|\)])",
            r"\1\3?\2\4",
            rule,
            count=1,
        )

        return rule

    def end_question(self, rule: str) -> str:
        """
        Applies transformations to the input rule string by replacing specific patterns.

        1. Replaces the first occurrence of 'PM|.*|P' with 'PM?'.
        2. Replaces the first occurrence of 'PM|P' with 'PM?'.
        3. Replaces the first occurrence of 'P|.*|PM' with 'PM?'.
        4. Replaces the first occurrence of 'P|PM' with 'PM?'.

        Args:
            rule (str): The input rule string to transform.

        Returns:
            str: The transformed rule string.
        """
        # Find all occurrences of PM|.*|P and replace with PM?
        rule = re.sub(
            r"([\|\(])([A-Za-z])([A-Za-z])\|(.*)\|\2([\|\)])",
            r"\1\2\3?|\4\5",
            rule,
            count=1,
        )
        # Find all occurrences of PM|P and replace with PM?
        rule = re.sub(
            r"([\|\(])([A-Za-z])([A-Za-z])\|\2([\|\)])",
            r"\1\2\3?\4",
            rule,
            count=1,
        )

        # Find all occurrences of P|.*|PM and replace with PM?
        rule = re.sub(
            r"([\|\(])([A-Za-z])\|(.*)\|\2([A-Za-z])([\|\)])",
            r"\1\2\4?|\3\5",
            rule,
            count=1,
        )
        # Find all occurrences of P|PM and replace with PM?
        rule = re.sub(
            r"([\|\(])([A-Za-z])\|\2([A-Za-z])([\|\)])",
            r"\1\2\3?\4",
            rule,
            count=1,
        )

        return rule

    def remove_duplicate_chars(self, rule: str) -> str:
        # Find all occurrences of HD|.*|HD and replace with HD|.*
        rule = re.sub(
            r"([\|\(])([A-Z]+)([\|\)])(.*)([\|\(])\2([\|\)])",
            r"\1\2\3\4\6",
            rule,
            count=1,
        )
        # Find all occurrences of HD|HD and replace with HD
        rule = re.sub(
            r"([\|\(])([A-Z]+)([\|\(])\2([\|\)])",
            r"\1\2\4",
            rule,
            count=1,
        )
        return rule

    def remove_double_asterisks(self, rule: str) -> str:
        rule = re.sub(r"(\.\*)+", r".*", rule)
        return rule

    def add_brackets(self, rule: str) -> str:

        # Replace PH|TH with [PT]H
        rule = re.sub(
            r"([\|\(])([A-Z])([A-Z])\|([A-Z])\3([\|\)])",
            r"\1[\2\4]\3\5",
            rule,
            count=1,
        )

        # Replace PH|.*|TH with [PT]H|.*
        rule = re.sub(
            r"([\|\(])([A-Z])([A-Z])\|(.*)\|([A-Z])\3([\|\)])",
            r"\1[\2\5]\3|\4\6",
            rule,
            count=1,
        )

        # Replace EL|EB with E[LB]
        rule = re.sub(
            r"([\|\(])([A-Z])([A-Z])\|\2([A-Z])([\|\)])",
            r"\1\2[\3\4]\5",
            rule,
            count=1,
        )

        # Replace EL|.*|EB with E[LB]|.*
        rule = re.sub(
            r"([\|\(])([A-Z])([A-Z])\|(.*)\|\2([A-Z])([\|\)])",
            r"\1\2[\3\5]|\4\6",
            rule,
            count=1,
        )

        return rule


class RegexleBoard:
    """
    A class for generating and managing regex-based structures.

    Attributes:
        REGEX_RULES_TEMPLATE (list): A list of regex rules templates used for generating random regex rules.
    """

    bit_offset = 6

    def __init__(
        self,
        side: int,
        day: int = -1,
        commit_hash: str = "NotSet",
        include_solution: bool = False,
        difficulty: float = 0.5,
        offset_override: int = None,
    ):
        """
        Initializes a RegexleBoard instance.

        Args:
            side (int): The side length of the board.
            seed (int): The seed for random number generation.
            include_solution (bool, optional): Whether to include the solution in the output. Defaults to False.
            difficulty (float, optional): Difficulty level for generating regex rules. Defaults to 0.5.
        """

        # Calculate parameters
        if offset_override:
            self.bit_offset = offset_override

        if side > 2**self.bit_offset:
            raise ValueError(
                f"{2**self.bit_offset} is the maximum side length, but was given: {side}"
            )

        self.seed = (day << self.bit_offset) + side
        random.seed(self.seed)

        self.regex_generator = RegexGenerator(self.seed, difficulty)
        self.include_solution = include_solution
        self.side = side
        self.day = day
        self.commit_hash = commit_hash
        self.diameter = 2 * self.side - 1
        self.length = self.diameter
        for n in range(self.side, self.diameter):
            self.length += 2 * n

        logging.info(f"Side: {self.side}")

        self._generate_structure()
        self._generate_y()
        self._generate_x()
        self._generate_z()
        self._cleanup_structure()
        self._generate_s()
        self._generate_regex()
        self.verify_regex()

    def random_char(self) -> str:

        return random.choice(string.ascii_uppercase)

    def get_unique_component(self, used: set, candidates: list) -> str:
        for candidate in candidates:
            if candidate not in used:
                used.add(candidate)
                return candidate
        return self.random_string()

    def _generate_structure(self):
        """
        Generates the structure of the board.
        """
        # Generate list of lengths
        self.lengths = []
        for i in range(self.side, self.diameter + 1):
            self.lengths.append(i)
        for i in range(self.diameter - 1, self.side - 1, -1):
            self.lengths.append(i)

        # Create array with sub-strings
        self.x = [[] for _ in range(self.diameter)]
        self.y = [[None for _ in range(self.diameter)] for _ in range(self.diameter)]
        self.z = [[] for _ in range(self.diameter)]

        self.x_str = ["" for _ in range(self.diameter)]
        self.y_str = ["" for _ in range(self.diameter)]
        self.z_str = ["" for _ in range(self.diameter)]

        self.x_regex = ["" for _ in range(self.diameter)]
        self.y_regex = ["" for _ in range(self.diameter)]
        self.z_regex = ["" for _ in range(self.diameter)]

    def _generate_y(self):
        """
        Generates the Y-axis strings for the board.
        """
        for i in range(self.diameter):
            for j in range(self.lengths[i]):
                self.y[i][j] = self.random_char()

    def _generate_x(self):
        """
        Generates the X-axis strings for the board.
        """
        # Copy array
        tmp = copy.deepcopy(self.y)

        # Iterate through lengths
        for i in range(self.diameter):
            length = self.lengths[i]
            counter = 0
            for j in range(self.diameter):
                if tmp[j][0] is not None:
                    self.x[i].append(tmp[j].pop(0))
                    counter += 1
                    if counter == length:
                        break

        # Reverse list and create string
        for i in range(self.diameter):
            self.x[i].reverse()

    def _generate_z(self):
        """
        Generates the Z-axis strings for the board.
        """
        # Copy array
        tmp = copy.deepcopy(self.y)

        # Move Nones from front to back
        for i in range(self.diameter):
            if i != (self.diameter // 2):
                # Find index of first None
                idx = tmp[i].index(None)

                # Swap parts of list around index
                tmp[i] = [*tmp[i][idx:], *tmp[i][:idx]]

        # Iterate through lengths
        for i in range(self.diameter):
            length = self.lengths[i]
            counter = 0
            for j in range(self.diameter):
                if tmp[j][-1] is not None:
                    self.z[i].append(tmp[j].pop(-1))
                    counter += 1
                    if counter == length:
                        break

        # Reverse list
        self.z.reverse()

    def _cleanup_structure(self):
        """
        Cleans up the structure of the board.
        """
        for i in range(self.diameter):
            for j in range(len(self.y[i]) - 1, -1, -1):
                if self.y[i][j] is None:
                    self.y[i].pop(j)

    def _generate_s(self):
        """
        Generates the full string for the board.
        """
        # Generate full string
        self.s = "".join(
            "".join(filter(None, row)) for row in self.y
        )  # Optimized full string generation
        self.y_str = ["".join(row) for row in self.y]
        self.x_str = ["".join(row) for row in self.x]  # Used list comprehension
        self.z_str = ["".join(row) for row in self.z]

    def print_board(self):
        """
        Prints the board structure.
        """
        print("Solution: \n")

        # Print out map
        for i in range(self.diameter):
            for j in range(self.diameter - self.lengths[i]):
                print(" ", end="")
            for j in range(self.lengths[i]):
                print(self.y[i][j] + " ", end="")
            print("")
        print("")

    def _generate_regex(self):
        """
        Generates regex rules for the board.
        """

        # Regex for y
        self.y_regex = self._generate_axis(self.y_str)
        logging.debug(f"REGEX: Y (left top to right top):\n {self.y_regex}")

        # Regex for x
        self.x_regex = self._generate_axis(self.x_str)
        logging.debug(f"REGEX: X (left middle to left top):\n {self.x_regex}")

        # Regex for z
        self.z_regex = self._generate_axis(self.z_str)
        logging.debug(f"REGEX: Z (left middle to left bottom):\n {self.z_regex}")

        # Generate json structure
        self.board = {
            "diameter": self.diameter,
            "name": f'"Day: {self.day} Side: {self.side}"',
            "day": self.day,
            "side": self.side,
            "seed": self.seed,
            "commit_hash": self.commit_hash,
            "x": self.x_regex,
            "y": self.y_regex,
            "z": self.z_regex,
        }

        if self.include_solution:
            self.board["solution"] = [x for x in self.y_str]

    def _generate_axis(self, axis_str: str) -> list[str]:

        return [self.regex_generator.random_regex(x) for x in axis_str]

    def save(self):
        """
        Saves the board to output.json
        """

        with open("output.json", "w") as f:
            json.dump(self.board, f, indent=4)

    def verify_regex(self):
        """
        Verifies the generated regex rules against the board strings.
        """

        for axis_str, axis_regex in [
            (self.x_str, self.x_regex),
            (self.y_str, self.y_regex),
            (self.z_str, self.z_regex),
        ]:
            self._verify_axis(axis_str, axis_regex)

    def _verify_axis(self, axis_str, axis_regex):
        for s, r in zip(axis_str, axis_regex):
            result = re.match(r, s)
            if result is None:
                logging.fatal(f"{r} does not match {s}")
                sys.exit()

        return True


class handler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.send_header("Access-Control-Allow-Origin", "*")
        self.send_header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
        self.end_headers()

        parsed_url = urlparse(self.path)
        query_params = parse_qs(parsed_url.query)

        day = int(query_params["day"][0]) if "day" in query_params else 1
        side = int(query_params["side"][0]) if "side" in query_params else 3
        include_solution = (
            bool(query_params["include_solution"][0])
            if "include_solution" in query_params
            else False
        )
        commit_hash = os.environ.get("VERCEL_GIT_COMMIT_SHA", "NotFound")[:8]

        logging.debug(self.path)
        logging.debug(type(day), type(side), type(include_solution))
        logging.debug(day, side, include_solution)

        if side > 32 or side <= 0:
            return

        board = RegexleBoard(
            side, day, commit_hash, include_solution=include_solution
        ).board

        self.wfile.write(json.dumps(board).encode("utf-8"))

        return


if __name__ == "__main__":
    from tqdm import tqdm

    logging.basicConfig(level=logging.ERROR, force=True)
    for i in tqdm(range(-10_000, 10_000)):
        board = RegexleBoard(3, i, include_solution=True)
