# Regexle Generator

This repository contains the source code for the Regexle Generator, which generates and serves Regexle game boards. The API is implemented using a Vercel worker and can be accessed through HTTP requests.

## Table of Contents

- [Regexle Generator](#regexle-generator)
  - [Table of Contents](#table-of-contents)
  - [Overview](#overview)
  - [Setup](#setup)
  - [Usage](#usage)
  - [Endpoints](#endpoints)
    - [GET /](#get-)
      - [Query Parameters](#query-parameters)
      - [Response](#response)
  - [Classes](#classes)
    - [RegexGenerator](#regexgenerator)
    - [RegexleBoard](#regexleboard)
## Overview

The Regexle Generator generates game boards where each board is represented by a set of strings and their corresponding regex patterns. The game board is dynamically created based on provided parameters such as the side length of the board, the day, and the difficulty level.

## Setup

To set up and run the Regexle Generator locally or on a Vercel worker, follow these steps:

1. **Clone the repository:**
   ```bash
   git clone https://github.com/Nathaniel.Belles/regexle_generator.git
   cd regexle_generator
   ```
2. **Run locally:**
   ```bash
   python3 api/index.py
   ```

3. **Deploy on Vercel:**
   The code is designed to run on a Vercel worker. Ensure you have the Vercel CLI installed and configured.
   ```bash
   vercel deploy
   ```

## Usage

Once deployed, the API can be accessed via HTTP requests. The main endpoint generates a new game board based on the provided query parameters.

## Endpoints

### GET /

Generates a Regexle game board.

#### Query Parameters

- `day` (optional, int): The day number to seed the random generator. Default is 1.
- `side` (optional, int): The side length of the board. Default is 3.
- `include_solution` (optional, bool): Whether to include the solution in the response. Default is False.

#### Response

Returns a JSON object representing the game board.

```json
{
  "diameter": 5,
  "name": "Day: 1 Side: 3",
  "day": 1,
  "side": 3,
  "seed": 193,
  "commit_hash": "current_commit_hash",
  "x": ["regex1", "regex2", ...],
  "y": ["regex3", "regex4", ...],
  "z": ["regex5", "regex6", ...],
  "solution": ["solution1", "solution2", ...] // Only if include_solution is True
}
```

## Classes

### RegexGenerator

Generates random regex rules based on a given string.

### RegexleBoard

Manages the creation and structure of the game board, including generating strings and their corresponding regex patterns.